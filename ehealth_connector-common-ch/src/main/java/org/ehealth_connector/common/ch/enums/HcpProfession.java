/*
 * The authorship of this project and accompanying materials is held by medshare GmbH, Switzerland.
 * All rights reserved. https://medshare.net
 *
 * Source code, documentation and other resources have been contributed by various people.
 * Project Team: https://gitlab.com/ehealth-connector/api/wikis/Team/
 * For exact developer information, please refer to the commit history of the forge.
 *
 * This code is made available under the terms of the Eclipse Public License v1.0.
 *
 * Accompanying materials are made available under the terms of the Creative Commons
 * Attribution-ShareAlike 4.0 License.
 *
 * This line is intended for UTF-8 encoding checks, do not modify/delete: äöüéè
 *
 */
package org.ehealth_connector.common.ch.enums;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

import org.ehealth_connector.common.enums.CodeSystems;
import org.ehealth_connector.common.enums.LanguageCode;
import org.ehealth_connector.common.mdht.enums.ValueSetEnumInterface;

/**
 * <!-- @formatter:off -->
 * <div class="en">no designation found for language ENGLISH</div>
 * <div class="de">no designation found for language GERMAN</div>
 * <div class="fr">no designation found for language FRENCH</div>
 * <div class="it">no designation found for language ITALIAN</div>
 * <!-- @formatter:on -->
 */
@Generated(value = "org.ehealth_connector.codegenerator.ch.valuesets.UpdateValueSets")
public enum HcpProfession implements ValueSetEnumInterface {

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Aktivierungsfachfrau/Aktivierungsfachmann</div>
	 * <div class="de">Aktivierungsfachfrau/Aktivierungsfachmann</div>
	 * <div class="fr">Spécialiste en activation</div>
	 * <div class="it">Specialista in attivazione</div>
	 * <!-- @formatter:on -->
	 */
	AKTIVIERUNGSFACHFRAU_AKTIVIERUNGSFACHMANN("00100", "2.16.756.5.30.1.127.3.10.9",
			"Aktivierungsfachfrau/Aktivierungsfachmann",
			"Aktivierungsfachfrau/Aktivierungsfachmann",
			"Aktivierungsfachfrau/Aktivierungsfachmann", "Spécialiste en activation",
			"Specialista in attivazione"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Biomedizinische Analytikerin/Biomedizinischer Analytiker</div>
	 * <div class="de">Biomedizinische Analytikerin/Biomedizinischer Analytiker</div>
	 * <div class="fr">Analyste biomédical</div>
	 * <div class="it">Tecnica in analisi biomediche/Tecnico in analisi biomediche</div>
	 * <!-- @formatter:on -->
	 */
	BIOMEDIZINISCHE_ANALYTIKERIN_BIOMEDIZINISCHER_ANALYTIKER("00300", "2.16.756.5.30.1.127.3.10.9",
			"Biomedizinische Analytikerin/Biomedizinischer Analytiker",
			"Biomedizinische Analytikerin/Biomedizinischer Analytiker",
			"Biomedizinische Analytikerin/Biomedizinischer Analytiker", "Analyste biomédical",
			"Tecnica in analisi biomediche/Tecnico in analisi biomediche"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Chiropractor (occupation)</div>
	 * <div class="de">TODO</div>
	 * <div class="fr">TODO</div>
	 * <div class="it">TODO</div>
	 * <!-- @formatter:on -->
	 */
	CHIROPRACTOR_OCCUPATION("3842006", "2.16.840.1.113883.6.96", "Chiropractor (occupation)",
			"Chiropractor (occupation)", "TODO", "TODO", "TODO"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dental hygienist</div>
	 * <div class="de">TODO</div>
	 * <div class="fr">TODO</div>
	 * <div class="it">TODO</div>
	 * <!-- @formatter:on -->
	 */
	DENTAL_HYGIENIST("00400", "2.16.756.5.30.1.127.3.10.9", "Dental hygienist", "Dental hygienist",
			"TODO", "TODO", "TODO"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dentist (occupation)</div>
	 * <div class="de">TODO</div>
	 * <div class="fr">TODO</div>
	 * <div class="it">TODO</div>
	 * <!-- @formatter:on -->
	 */
	DENTIST_OCCUPATION("106289002", "2.16.840.1.113883.6.96", "Dentist (occupation)",
			"Dentist (occupation)", "TODO", "TODO", "TODO"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dietitian (occupation)</div>
	 * <div class="de">TODO</div>
	 * <div class="fr">TODO</div>
	 * <div class="it">TODO</div>
	 * <!-- @formatter:on -->
	 */
	DIETITIAN_OCCUPATION("159033005", "2.16.840.1.113883.6.96", "Dietitian (occupation)",
			"Dietitian (occupation)", "TODO", "TODO", "TODO"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Drogist/Drogistin</div>
	 * <div class="de">Drogist/Drogistin</div>
	 * <div class="fr">Droguiste</div>
	 * <div class="it">Droghiere/a</div>
	 * <!-- @formatter:on -->
	 */
	DROGIST_DROGISTIN("00500", "2.16.756.5.30.1.127.3.10.9", "Drogist/Drogistin",
			"Drogist/Drogistin", "Drogist/Drogistin", "Droguiste", "Droghiere/a"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fachfrau für medizinisch-technische Radiologie/Fachmann für medizinisch-technische Radiologie</div>
	 * <div class="de">Fachfrau für medizinisch-technische Radiologie/Fachmann für medizinisch-technische Radiologie</div>
	 * <div class="fr">Technicien/ne en radiologie médicale</div>
	 * <div class="it">Tecnica di radiologia medica/Tecnico di radiologia medica</div>
	 * <!-- @formatter:on -->
	 */
	FACHFRAU_F_R_MEDIZINISCH_TECHNISCHE_RADIOLOGIE_FACHMANN_F_R_MEDIZINISCH_TECHNISCHE_RADIOLOGIE(
			"00600", "2.16.756.5.30.1.127.3.10.9",
			"Fachfrau für medizinisch-technische Radiologie/Fachmann für medizinisch-technische Radiologie",
			"Fachfrau für medizinisch-technische Radiologie/Fachmann für medizinisch-technische Radiologie",
			"Fachfrau für medizinisch-technische Radiologie/Fachmann für medizinisch-technische Radiologie",
			"Technicien/ne en radiologie médicale",
			"Tecnica di radiologia medica/Tecnico di radiologia medica"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fachfrau Gesundheit/Fachmann Gesundheit</div>
	 * <div class="de">Fachfrau Gesundheit/Fachmann Gesundheit</div>
	 * <div class="fr">Assistant/e en soins et santé communautaire</div>
	 * <div class="it">Operatrice sociosanitaria/Operatore sociosanitario</div>
	 * <!-- @formatter:on -->
	 */
	FACHFRAU_GESUNDHEIT_FACHMANN_GESUNDHEIT("00700", "2.16.756.5.30.1.127.3.10.9",
			"Fachfrau Gesundheit/Fachmann Gesundheit", "Fachfrau Gesundheit/Fachmann Gesundheit",
			"Fachfrau Gesundheit/Fachmann Gesundheit",
			"Assistant/e en soins et santé communautaire",
			"Operatrice sociosanitaria/Operatore sociosanitario"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fachfrau Operationstechnik/Fachmann Operationstechnik</div>
	 * <div class="de">Fachfrau Operationstechnik/Fachmann Operationstechnik</div>
	 * <div class="fr">Technicien/ne en salle d’opération</div>
	 * <div class="it">Tecnica di sala operatoria/Tecnico di sala operatoria</div>
	 * <!-- @formatter:on -->
	 */
	FACHFRAU_OPERATIONSTECHNIK_FACHMANN_OPERATIONSTECHNIK("00800", "2.16.756.5.30.1.127.3.10.9",
			"Fachfrau Operationstechnik/Fachmann Operationstechnik",
			"Fachfrau Operationstechnik/Fachmann Operationstechnik",
			"Fachfrau Operationstechnik/Fachmann Operationstechnik",
			"Technicien/ne en salle d’opération",
			"Tecnica di sala operatoria/Tecnico di sala operatoria"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Medizinische Masseurin/Medizinischer Masseur</div>
	 * <div class="de">Medizinische Masseurin/Medizinischer Masseur</div>
	 * <div class="fr">Masseur médical/masseuse médicale</div>
	 * <div class="it">Massaggiatrice medicale/Massaggiatore medicale</div>
	 * <!-- @formatter:on -->
	 */
	MEDIZINISCHE_MASSEURIN_MEDIZINISCHER_MASSEUR("00900", "2.16.756.5.30.1.127.3.10.9",
			"Medizinische Masseurin/Medizinischer Masseur",
			"Medizinische Masseurin/Medizinischer Masseur",
			"Medizinische Masseurin/Medizinischer Masseur", "Masseur médical/masseuse médicale",
			"Massaggiatrice medicale/Massaggiatore medicale"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Naturheilpraktikerin/Naturheilpraktiker</div>
	 * <div class="de">Naturheilpraktikerin/Naturheilpraktiker</div>
	 * <div class="fr">Naturopathe</div>
	 * <div class="it">Naturopata</div>
	 * <!-- @formatter:on -->
	 */
	NATURHEILPRAKTIKERIN_NATURHEILPRAKTIKER("01000", "2.16.756.5.30.1.127.3.10.9",
			"Naturheilpraktikerin/Naturheilpraktiker", "Naturheilpraktikerin/Naturheilpraktiker",
			"Naturheilpraktikerin/Naturheilpraktiker", "Naturopathe", "Naturopata"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Occupational therapist (occupation)</div>
	 * <div class="de">TODO</div>
	 * <div class="fr">TODO</div>
	 * <div class="it">TODO</div>
	 * <!-- @formatter:on -->
	 */
	OCCUPATIONAL_THERAPIST_OCCUPATION("80546007", "2.16.840.1.113883.6.96",
			"Occupational therapist (occupation)", "Occupational therapist (occupation)", "TODO",
			"TODO", "TODO"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Optician</div>
	 * <div class="de">TODO</div>
	 * <div class="fr">TODO</div>
	 * <div class="it">TODO</div>
	 * <!-- @formatter:on -->
	 */
	OPTICIAN("00200", "2.16.756.5.30.1.127.3.10.9", "Optician", "Optician", "TODO", "TODO", "TODO"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Optometrist</div>
	 * <div class="de">Optometrist</div>
	 * <div class="fr">TODO</div>
	 * <div class="it">TODO</div>
	 * <!-- @formatter:on -->
	 */
	OPTOMETRIST("01100", "2.16.756.5.30.1.127.3.10.9", "Optometrist", "Optometrist", "Optometrist",
			"TODO", "TODO"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Orthoptist</div>
	 * <div class="de">Orthoptist</div>
	 * <div class="fr">TODO</div>
	 * <div class="it">TODO</div>
	 * <!-- @formatter:on -->
	 */
	ORTHOPTIST("01200", "2.16.756.5.30.1.127.3.10.9", "Orthoptist", "Orthoptist", "Orthoptist",
			"TODO", "TODO"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Osteopath</div>
	 * <div class="de">Osteopath</div>
	 * <div class="fr">TODO</div>
	 * <div class="it">TODO</div>
	 * <!-- @formatter:on -->
	 */
	OSTEOPATH("01300", "2.16.756.5.30.1.127.3.10.9", "Osteopath", "Osteopath", "Osteopath", "TODO",
			"TODO"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Other</div>
	 * <div class="de">Andere</div>
	 * <div class="fr">Autre</div>
	 * <div class="it">Altre</div>
	 * <!-- @formatter:on -->
	 */
	OTHER("00000", "2.16.756.5.30.1.127.3.10.9", "Other", "Other", "Andere", "Autre", "Altre"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pharmacist (occupation)</div>
	 * <div class="de">TODO</div>
	 * <div class="fr">TODO</div>
	 * <div class="it">TODO</div>
	 * <!-- @formatter:on -->
	 */
	PHARMACIST_OCCUPATION("46255001", "2.16.840.1.113883.6.96", "Pharmacist (occupation)",
			"Pharmacist (occupation)", "TODO", "TODO", "TODO"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Physician (occupation)</div>
	 * <div class="de">TODO</div>
	 * <div class="fr">TODO</div>
	 * <div class="it">TODO</div>
	 * <!-- @formatter:on -->
	 */
	PHYSICIAN_OCCUPATION("309343006", "2.16.840.1.113883.6.96", "Physician (occupation)",
			"Physician (occupation)", "TODO", "TODO", "TODO"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Physiotherapist (occupation)</div>
	 * <div class="de">TODO</div>
	 * <div class="fr">TODO</div>
	 * <div class="it">TODO</div>
	 * <!-- @formatter:on -->
	 */
	PHYSIOTHERAPIST_OCCUPATION("36682004", "2.16.840.1.113883.6.96", "Physiotherapist (occupation)",
			"Physiotherapist (occupation)", "TODO", "TODO", "TODO"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Podologin/Podologe</div>
	 * <div class="de">Podologin/Podologe</div>
	 * <div class="fr">Ambulancier/ambulancière</div>
	 * <div class="it">Podologa/Podologo</div>
	 * <!-- @formatter:on -->
	 */
	PODOLOGIN_PODOLOGE("01400", "2.16.756.5.30.1.127.3.10.9", "Podologin/Podologe",
			"Podologin/Podologe", "Podologin/Podologe", "Ambulancier/ambulancière",
			"Podologa/Podologo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Professional nurse (occupation)</div>
	 * <div class="de">TODO</div>
	 * <div class="fr">TODO</div>
	 * <div class="it">TODO</div>
	 * <!-- @formatter:on -->
	 */
	PROFESSIONAL_NURSE_OCCUPATION("106292003", "2.16.840.1.113883.6.96",
			"Professional nurse (occupation)", "Professional nurse (occupation)", "TODO", "TODO",
			"TODO"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Psychologist (occupation)</div>
	 * <div class="de">TODO</div>
	 * <div class="fr">TODO</div>
	 * <div class="it">TODO</div>
	 * <!-- @formatter:on -->
	 */
	PSYCHOLOGIST_OCCUPATION("59944000", "2.16.840.1.113883.6.96", "Psychologist (occupation)",
			"Psychologist (occupation)", "TODO", "TODO", "TODO"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Registered midwife (occupation)</div>
	 * <div class="de">TODO</div>
	 * <div class="fr">TODO</div>
	 * <div class="it">TODO</div>
	 * <!-- @formatter:on -->
	 */
	REGISTERED_MIDWIFE_OCCUPATION("309453006", "2.16.840.1.113883.6.96",
			"Registered midwife (occupation)", "Registered midwife (occupation)", "TODO", "TODO",
			"TODO"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rettungssanitäterin/Rettungssanitäter</div>
	 * <div class="de">Rettungssanitäterin/Rettungssanitäter</div>
	 * <div class="fr">Ambulancier/ambulancière</div>
	 * <div class="it">Soccorritrice/Soccorritore</div>
	 * <!-- @formatter:on -->
	 */
	RETTUNGSSANIT_TERIN_RETTUNGSSANIT_TER("01500", "2.16.756.5.30.1.127.3.10.9",
			"Rettungssanitäterin/Rettungssanitäter", "Rettungssanitäterin/Rettungssanitäter",
			"Rettungssanitäterin/Rettungssanitäter", "Ambulancier/ambulancière",
			"Soccorritrice/Soccorritore"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Speech/language therapist (occupation)</div>
	 * <div class="de">TODO</div>
	 * <div class="fr">TODO</div>
	 * <div class="it">TODO</div>
	 * <!-- @formatter:on -->
	 */
	SPEECH_LANGUAGE_THERAPIST_OCCUPATION("159026005", "2.16.840.1.113883.6.96",
			"Speech/language therapist (occupation)", "Speech/language therapist (occupation)",
			"TODO", "TODO", "TODO");

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Aktivierungsfachfrau/Aktivierungsfachmann</div>
	 * <div class="de">Code für Aktivierungsfachfrau/Aktivierungsfachmann</div>
	 * <div class="fr">Code de Spécialiste en activation</div>
	 * <div class="it">Code per Specialista in attivazione</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AKTIVIERUNGSFACHFRAU_AKTIVIERUNGSFACHMANN_CODE = "00100";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Biomedizinische Analytikerin/Biomedizinischer Analytiker</div>
	 * <div class="de">Code für Biomedizinische Analytikerin/Biomedizinischer Analytiker</div>
	 * <div class="fr">Code de Analyste biomédical</div>
	 * <div class="it">Code per Tecnica in analisi biomediche/Tecnico in analisi biomediche</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BIOMEDIZINISCHE_ANALYTIKERIN_BIOMEDIZINISCHER_ANALYTIKER_CODE = "00300";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Chiropractor (occupation)</div>
	 * <div class="de">Code für TODO</div>
	 * <div class="fr">Code de TODO</div>
	 * <div class="it">Code per TODO</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CHIROPRACTOR_OCCUPATION_CODE = "3842006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dental hygienist</div>
	 * <div class="de">Code für TODO</div>
	 * <div class="fr">Code de TODO</div>
	 * <div class="it">Code per TODO</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DENTAL_HYGIENIST_CODE = "00400";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dentist (occupation)</div>
	 * <div class="de">Code für TODO</div>
	 * <div class="fr">Code de TODO</div>
	 * <div class="it">Code per TODO</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DENTIST_OCCUPATION_CODE = "106289002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dietitian (occupation)</div>
	 * <div class="de">Code für TODO</div>
	 * <div class="fr">Code de TODO</div>
	 * <div class="it">Code per TODO</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIETITIAN_OCCUPATION_CODE = "159033005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Drogist/Drogistin</div>
	 * <div class="de">Code für Drogist/Drogistin</div>
	 * <div class="fr">Code de Droguiste</div>
	 * <div class="it">Code per Droghiere/a</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DROGIST_DROGISTIN_CODE = "00500";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fachfrau für medizinisch-technische Radiologie/Fachmann für medizinisch-technische Radiologie</div>
	 * <div class="de">Code für Fachfrau für medizinisch-technische Radiologie/Fachmann für medizinisch-technische Radiologie</div>
	 * <div class="fr">Code de Technicien/ne en radiologie médicale</div>
	 * <div class="it">Code per Tecnica di radiologia medica/Tecnico di radiologia medica</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FACHFRAU_F_R_MEDIZINISCH_TECHNISCHE_RADIOLOGIE_FACHMANN_F_R_MEDIZINISCH_TECHNISCHE_RADIOLOGIE_CODE = "00600";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fachfrau Gesundheit/Fachmann Gesundheit</div>
	 * <div class="de">Code für Fachfrau Gesundheit/Fachmann Gesundheit</div>
	 * <div class="fr">Code de Assistant/e en soins et santé communautaire</div>
	 * <div class="it">Code per Operatrice sociosanitaria/Operatore sociosanitario</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FACHFRAU_GESUNDHEIT_FACHMANN_GESUNDHEIT_CODE = "00700";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fachfrau Operationstechnik/Fachmann Operationstechnik</div>
	 * <div class="de">Code für Fachfrau Operationstechnik/Fachmann Operationstechnik</div>
	 * <div class="fr">Code de Technicien/ne en salle d’opération</div>
	 * <div class="it">Code per Tecnica di sala operatoria/Tecnico di sala operatoria</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FACHFRAU_OPERATIONSTECHNIK_FACHMANN_OPERATIONSTECHNIK_CODE = "00800";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Medizinische Masseurin/Medizinischer Masseur</div>
	 * <div class="de">Code für Medizinische Masseurin/Medizinischer Masseur</div>
	 * <div class="fr">Code de Masseur médical/masseuse médicale</div>
	 * <div class="it">Code per Massaggiatrice medicale/Massaggiatore medicale</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MEDIZINISCHE_MASSEURIN_MEDIZINISCHER_MASSEUR_CODE = "00900";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Naturheilpraktikerin/Naturheilpraktiker</div>
	 * <div class="de">Code für Naturheilpraktikerin/Naturheilpraktiker</div>
	 * <div class="fr">Code de Naturopathe</div>
	 * <div class="it">Code per Naturopata</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NATURHEILPRAKTIKERIN_NATURHEILPRAKTIKER_CODE = "01000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Occupational therapist (occupation)</div>
	 * <div class="de">Code für TODO</div>
	 * <div class="fr">Code de TODO</div>
	 * <div class="it">Code per TODO</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OCCUPATIONAL_THERAPIST_OCCUPATION_CODE = "80546007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Optician</div>
	 * <div class="de">Code für TODO</div>
	 * <div class="fr">Code de TODO</div>
	 * <div class="it">Code per TODO</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OPTICIAN_CODE = "00200";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Optometrist</div>
	 * <div class="de">Code für Optometrist</div>
	 * <div class="fr">Code de TODO</div>
	 * <div class="it">Code per TODO</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OPTOMETRIST_CODE = "01100";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Orthoptist</div>
	 * <div class="de">Code für Orthoptist</div>
	 * <div class="fr">Code de TODO</div>
	 * <div class="it">Code per TODO</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ORTHOPTIST_CODE = "01200";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Osteopath</div>
	 * <div class="de">Code für Osteopath</div>
	 * <div class="fr">Code de TODO</div>
	 * <div class="it">Code per TODO</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OSTEOPATH_CODE = "01300";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Other</div>
	 * <div class="de">Code für Andere</div>
	 * <div class="fr">Code de Autre</div>
	 * <div class="it">Code per Altre</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OTHER_CODE = "00000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pharmacist (occupation)</div>
	 * <div class="de">Code für TODO</div>
	 * <div class="fr">Code de TODO</div>
	 * <div class="it">Code per TODO</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHARMACIST_OCCUPATION_CODE = "46255001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Physician (occupation)</div>
	 * <div class="de">Code für TODO</div>
	 * <div class="fr">Code de TODO</div>
	 * <div class="it">Code per TODO</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHYSICIAN_OCCUPATION_CODE = "309343006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Physiotherapist (occupation)</div>
	 * <div class="de">Code für TODO</div>
	 * <div class="fr">Code de TODO</div>
	 * <div class="it">Code per TODO</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHYSIOTHERAPIST_OCCUPATION_CODE = "36682004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Podologin/Podologe</div>
	 * <div class="de">Code für Podologin/Podologe</div>
	 * <div class="fr">Code de Ambulancier/ambulancière</div>
	 * <div class="it">Code per Podologa/Podologo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PODOLOGIN_PODOLOGE_CODE = "01400";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Professional nurse (occupation)</div>
	 * <div class="de">Code für TODO</div>
	 * <div class="fr">Code de TODO</div>
	 * <div class="it">Code per TODO</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROFESSIONAL_NURSE_OCCUPATION_CODE = "106292003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Psychologist (occupation)</div>
	 * <div class="de">Code für TODO</div>
	 * <div class="fr">Code de TODO</div>
	 * <div class="it">Code per TODO</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PSYCHOLOGIST_OCCUPATION_CODE = "59944000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Registered midwife (occupation)</div>
	 * <div class="de">Code für TODO</div>
	 * <div class="fr">Code de TODO</div>
	 * <div class="it">Code per TODO</div>
	 * <!-- @formatter:on -->
	 */
	public static final String REGISTERED_MIDWIFE_OCCUPATION_CODE = "309453006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rettungssanitäterin/Rettungssanitäter</div>
	 * <div class="de">Code für Rettungssanitäterin/Rettungssanitäter</div>
	 * <div class="fr">Code de Ambulancier/ambulancière</div>
	 * <div class="it">Code per Soccorritrice/Soccorritore</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RETTUNGSSANIT_TERIN_RETTUNGSSANIT_TER_CODE = "01500";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Speech/language therapist (occupation)</div>
	 * <div class="de">Code für TODO</div>
	 * <div class="fr">Code de TODO</div>
	 * <div class="it">Code per TODO</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SPEECH_LANGUAGE_THERAPIST_OCCUPATION_CODE = "159026005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Identifier of the value set</div>
	 * <div class="de">Identifikator für das Value Set</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VALUE_SET_ID = "2.16.756.5.30.1.127.3.10.8.1";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Name of the value set</div>
	 * <div class="de">Name des Value Sets</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VALUE_SET_NAME = "HCProfessional.hcProfession";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the Enum with a given code</div>
	 * <div class="de">Liefert den Enum anhand eines gegebenen codes</div>
	 * <!-- @formatter:on -->
	 *
	 * @param code
	 *            <div class="de"> code</div>
	 * @return <div class="en">the enum</div>
	 */
	public static HcpProfession getEnum(String code) {
		for (final HcpProfession x : values()) {
			if (x.getCodeValue().equals(code)) {
				return x;
			}
		}
		return null;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Checks if a given enum is part of this value set.</div>
	 * <div class="de">Prüft, ob der angegebene enum Teil dieses Value Sets ist.</div>
	 * <!-- @formatter:on -->
	 *
	 * @param enumName
	 *            <div class="de"> enumName</div>
	 * @return true, if enum is in this value set
	 */
	public static boolean isEnumOfValueSet(String enumName) {
		if (enumName == null) {
			return false;
		}
		try {
			Enum.valueOf(HcpProfession.class, enumName);
			return true;
		} catch (final IllegalArgumentException ex) {
			return false;
		}
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Checks if a given code value is in this value set.</div>
	 * <div class="de">Prüft, ob der angegebene code in diesem Value Set vorhanden ist.</div>
	 * <!-- @formatter:on -->
	 *
	 * @param codeValue
	 *            <div class="de"> code</div>
	 * @return true, if is in value set
	 */
	public static boolean isInValueSet(String codeValue) {
		for (final HcpProfession x : values()) {
			if (x.getCodeValue().equals(codeValue)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Machine interpretable and (inside this class) unique code</div>
	 * <div class="de">Maschinen interpretierbarer und (innerhalb dieser Klasse) eindeutiger Code</div>
	 * <!-- @formatter:on -->
	 */
	private String code;

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Identifier of the referencing code system.</div>
	 * <div class="de">Identifikator des referenzierende Codesystems.</div>
	 * <!-- @formatter:on -->
	 */
	private String codeSystem;

	/**
	 * The display names per language
	 */
	private Map<LanguageCode, String> displayNames;

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Instantiates this Enum Object with a given Code and Display Name</div>
	 * <div class="de">Instanziiert dieses Enum Object mittels eines Codes und einem Display Name</div>.
	 * <!-- @formatter:on -->
	 *
	 * @param code
	 *            code
	 * @param codeSystem
	 *            codeSystem
	 * @param displayName
	 *            the default display name
	 * @param displayNameEn
	 *            the display name en
	 * @param displayNameDe
	 *            the display name de
	 * @param displayNameFr
	 *            the display name fr
	 * @param displayNameIt
	 *            the display name it
	 */
	HcpProfession(String code, String codeSystem, String displayName, String displayNameEn,
			String displayNameDe, String displayNameFr, String displayNameIt) {
		this.code = code;
		this.codeSystem = codeSystem;
		displayNames = new HashMap<>();
		displayNames.put(null, displayName);
		displayNames.put(LanguageCode.ENGLISH, displayNameEn);
		displayNames.put(LanguageCode.GERMAN, displayNameDe);
		displayNames.put(LanguageCode.FRENCH, displayNameFr);
		displayNames.put(LanguageCode.ITALIAN, displayNameIt);
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the code system identifier.</div>
	 * <div class="de">Liefert den Code System Identifikator.</div>
	 * <!-- @formatter:on -->
	 *
	 * @return <div class="en">the code system identifier</div>
	 */
	@Override
	public String getCodeSystemId() {
		return this.codeSystem;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the code system name.</div>
	 * <div class="de">Liefert den Code System Namen.</div>
	 * <!-- @formatter:on -->
	 *
	 * @return <div class="en">the code system identifier</div>
	 */
	@Override
	public String getCodeSystemName() {
		String retVal = "";
		CodeSystems cs = CodeSystems.getEnum(this.codeSystem);
		if (cs != null)
			retVal = cs.getCodeSystemName();
		return retVal;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the actual Code as string</div>
	 * <div class="de">Liefert den eigentlichen Code als String</div>
	 * <!-- @formatter:on -->
	 *
	 * @return <div class="en">the code</div>
	 */
	@Override
	public String getCodeValue() {
		return this.code;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the display name defined by the language param. If
	 * there is no english translation, the default display name is returned.</div>
	 * <div class="de">Liefert display name gemäss Parameter, falls es keine
	 * Englische Übersetzung gibt, wird der default-Name zurückgegeben.</div>
	 * <!-- @formatter:on -->
	 *
	 * @param languageCode
	 *            the language code to get the display name for
	 * @return returns the display name in the desired language. if language not
	 *         found, display name in german will returned
	 */
	@Override
	public String getDisplayName(LanguageCode languageCode) {
		String displayName = displayNames.get(languageCode);
		if (displayName == null && languageCode == LanguageCode.ENGLISH) {
			return displayNames.get(null);
		}
		return displayName;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the value set identifier.</div>
	 * <div class="de">Liefert den Value Set Identifikator.</div>
	 * <!-- @formatter:on -->
	 *
	 * @return <div class="en">the value set identifier</div>
	 */
	@Override
	public String getValueSetId() {
		return VALUE_SET_ID;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the value set name.</div>
	 * <div class="de">Liefert den Value Set Namen.</div>
	 * <!-- @formatter:on -->
	 *
	 * @return <div class="en">the value set name</div>
	 */
	@Override
	public String getValueSetName() {
		return VALUE_SET_NAME;
	}
}
