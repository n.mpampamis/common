/*
 * The authorship of this project and accompanying materials is held by medshare GmbH, Switzerland.
 * All rights reserved. https://medshare.net
 *
 * Source code, documentation and other resources have been contributed by various people.
 * Project Team: https://gitlab.com/ehealth-connector/api/wikis/Team/
 * For exact developer information, please refer to the commit history of the forge.
 *
 * This code is made available under the terms of the Eclipse Public License v1.0.
 *
 * Accompanying materials are made available under the terms of the Creative Commons
 * Attribution-ShareAlike 4.0 License.
 *
 * This line is intended for UTF-8 encoding checks, do not modify/delete: äöüéè
 *
 */
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren.
// Generiert: 2019.04.13 um 06:23:49 PM CEST
//

package org.ehealth_connector.common.hl7cdar2;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java-Klasse für SLIST_PQ complex type.
 *
 * <p>
 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
 * Klasse enthalten ist.
 *
 * <pre>
 * &lt;complexType name="SLIST_PQ">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:hl7-org:v3}ANY">
 *       &lt;sequence>
 *         &lt;element name="origin" type="{urn:hl7-org:v3}PQ"/>
 *         &lt;element name="scale" type="{urn:hl7-org:v3}PQ"/>
 *         &lt;element name="digits" type="{urn:hl7-org:v3}list_int"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SLIST_PQ", propOrder = { "origin", "scale", "digits" })
public class SLISTPQ extends ANY {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7521852679975386697L;
	@XmlElement(required = true)
	protected PQ origin;
	@XmlElement(required = true)
	protected PQ scale;
	@XmlList
	@XmlElement(required = true)
	protected List<BigInteger> digits;

	/**
	 * Gets the value of the digits property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the digits property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getDigits().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link BigInteger }
	 *
	 *
	 */
	public List<BigInteger> getDigits() {
		if (digits == null) {
			digits = new ArrayList<BigInteger>();
		}
		return this.digits;
	}

	/**
	 * Ruft den Wert der origin-Eigenschaft ab.
	 *
	 * @return possible object is {@link PQ }
	 *
	 */
	public PQ getOrigin() {
		return origin;
	}

	/**
	 * Ruft den Wert der scale-Eigenschaft ab.
	 *
	 * @return possible object is {@link PQ }
	 *
	 */
	public PQ getScale() {
		return scale;
	}

	/**
	 * Legt den Wert der origin-Eigenschaft fest.
	 *
	 * @param value
	 *            allowed object is {@link PQ }
	 *
	 */
	public void setOrigin(PQ value) {
		this.origin = value;
	}

	/**
	 * Legt den Wert der scale-Eigenschaft fest.
	 *
	 * @param value
	 *            allowed object is {@link PQ }
	 *
	 */
	public void setScale(PQ value) {
		this.scale = value;
	}

}
