/*
 * The authorship of this project and accompanying materials is held by medshare GmbH, Switzerland.
 * All rights reserved. https://medshare.net
 *
 * Source code, documentation and other resources have been contributed by various people.
 * Project Team: https://gitlab.com/ehealth-connector/api/wikis/Team/
 * For exact developer information, please refer to the commit history of the forge.
 *
 * This code is made available under the terms of the Eclipse Public License v1.0.
 *
 * Accompanying materials are made available under the terms of the Creative Commons
 * Attribution-ShareAlike 4.0 License.
 *
 * This line is intended for UTF-8 encoding checks, do not modify/delete: äöüéè
 *
 */
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren.
// Generiert: 2019.04.13 um 06:23:49 PM CEST
//

package org.ehealth_connector.common.hl7cdar2;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java-Klasse für POCD_MT000040.Observation complex type.
 *
 * <p>
 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser
 * Klasse enthalten ist.
 *
 * <pre>
 * &lt;complexType name="POCD_MT000040.Observation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="realmCode" type="{urn:hl7-org:v3}CS" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="typeId" type="{urn:hl7-org:v3}POCD_MT000040.InfrastructureRoot.typeId" minOccurs="0"/>
 *         &lt;element name="templateId" type="{urn:hl7-org:v3}II" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="id" type="{urn:hl7-org:v3}II" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="code" type="{urn:hl7-org:v3}CD"/>
 *         &lt;element name="derivationExpr" type="{urn:hl7-org:v3}ST" minOccurs="0"/>
 *         &lt;element name="text" type="{urn:hl7-org:v3}ED" minOccurs="0"/>
 *         &lt;element name="statusCode" type="{urn:hl7-org:v3}CS" minOccurs="0"/>
 *         &lt;element name="effectiveTime" type="{urn:hl7-org:v3}IVL_TS" minOccurs="0"/>
 *         &lt;element name="priorityCode" type="{urn:hl7-org:v3}CE" minOccurs="0"/>
 *         &lt;element name="repeatNumber" type="{urn:hl7-org:v3}IVL_INT" minOccurs="0"/>
 *         &lt;element name="languageCode" type="{urn:hl7-org:v3}CS" minOccurs="0"/>
 *         &lt;element name="value" type="{urn:hl7-org:v3}ANY" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="interpretationCode" type="{urn:hl7-org:v3}CE" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="methodCode" type="{urn:hl7-org:v3}CE" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="targetSiteCode" type="{urn:hl7-org:v3}CD" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="subject" type="{urn:hl7-org:v3}POCD_MT000040.Subject" minOccurs="0"/>
 *         &lt;element name="specimen" type="{urn:hl7-org:v3}POCD_MT000040.Specimen" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="performer" type="{urn:hl7-org:v3}POCD_MT000040.Performer2" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="author" type="{urn:hl7-org:v3}POCD_MT000040.Author" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="informant" type="{urn:hl7-org:v3}POCD_MT000040.Informant12" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="participant" type="{urn:hl7-org:v3}POCD_MT000040.Participant2" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="entryRelationship" type="{urn:hl7-org:v3}POCD_MT000040.EntryRelationship" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="reference" type="{urn:hl7-org:v3}POCD_MT000040.Reference" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="precondition" type="{urn:hl7-org:v3}POCD_MT000040.Precondition" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="referenceRange" type="{urn:hl7-org:v3}POCD_MT000040.ReferenceRange" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="nullFlavor" type="{urn:hl7-org:v3}NullFlavor" />
 *       &lt;attribute name="classCode" use="required" type="{urn:hl7-org:v3}ActClassObservation" />
 *       &lt;attribute name="moodCode" use="required" type="{urn:hl7-org:v3}x_ActMoodDocumentObservation" />
 *       &lt;attribute name="negationInd" type="{urn:hl7-org:v3}bl" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "POCD_MT000040.Observation", propOrder = { "realmCode", "typeId", "templateId",
		"id", "code", "derivationExpr", "text", "statusCode", "effectiveTime", "priorityCode",
		"repeatNumber", "languageCode", "value", "interpretationCode", "methodCode",
		"targetSiteCode", "subject", "specimen", "performer", "author", "informant", "participant",
		"entryRelationship", "reference", "precondition", "referenceRange" })
public class POCDMT000040Observation {

	protected List<CS> realmCode;
	protected POCDMT000040InfrastructureRootTypeId typeId;
	protected List<II> templateId;
	protected List<II> id;
	@XmlElement(required = true)
	protected CD code;
	protected ST derivationExpr;
	protected ED text;
	protected CS statusCode;
	protected IVLTS effectiveTime;
	protected CE priorityCode;
	protected IVLINT repeatNumber;
	protected CS languageCode;
	protected List<ANY> value;
	protected List<CE> interpretationCode;
	protected List<CE> methodCode;
	protected List<CD> targetSiteCode;
	protected POCDMT000040Subject subject;
	protected List<POCDMT000040Specimen> specimen;
	protected List<POCDMT000040Performer2> performer;
	protected List<POCDMT000040Author> author;
	protected List<POCDMT000040Informant12> informant;
	protected List<POCDMT000040Participant2> participant;
	protected List<POCDMT000040EntryRelationship> entryRelationship;
	protected List<POCDMT000040Reference> reference;
	protected List<POCDMT000040Precondition> precondition;
	protected List<POCDMT000040ReferenceRange> referenceRange;
	@XmlAttribute(name = "nullFlavor")
	protected List<String> nullFlavor;
	@XmlAttribute(name = "classCode", required = true)
	protected List<String> classCode;
	@XmlAttribute(name = "moodCode", required = true)
	protected XActMoodDocumentObservation moodCode;
	@XmlAttribute(name = "negationInd")
	protected Boolean negationInd;

	/**
	 * Gets the value of the author property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the author property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getAuthor().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link POCDMT000040Author }
	 *
	 *
	 */
	public List<POCDMT000040Author> getAuthor() {
		if (author == null) {
			author = new ArrayList<POCDMT000040Author>();
		}
		return this.author;
	}

	/**
	 * Gets the value of the classCode property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the classCode property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getClassCode().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link String }
	 *
	 *
	 */
	public List<String> getClassCode() {
		if (classCode == null) {
			classCode = new ArrayList<String>();
		}
		return this.classCode;
	}

	/**
	 * Ruft den Wert der code-Eigenschaft ab.
	 *
	 * @return possible object is {@link CD }
	 *
	 */
	public CD getCode() {
		return code;
	}

	/**
	 * Ruft den Wert der derivationExpr-Eigenschaft ab.
	 *
	 * @return possible object is {@link ST }
	 *
	 */
	public ST getDerivationExpr() {
		return derivationExpr;
	}

	/**
	 * Ruft den Wert der effectiveTime-Eigenschaft ab.
	 *
	 * @return possible object is {@link IVLTS }
	 *
	 */
	public IVLTS getEffectiveTime() {
		return effectiveTime;
	}

	/**
	 * Gets the value of the entryRelationship property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the entryRelationship property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getEntryRelationship().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link POCDMT000040EntryRelationship }
	 *
	 *
	 */
	public List<POCDMT000040EntryRelationship> getEntryRelationship() {
		if (entryRelationship == null) {
			entryRelationship = new ArrayList<POCDMT000040EntryRelationship>();
		}
		return this.entryRelationship;
	}

	/**
	 * Gets the value of the id property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the id property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getId().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link II }
	 *
	 *
	 */
	public List<II> getId() {
		if (id == null) {
			id = new ArrayList<II>();
		}
		return this.id;
	}

	/**
	 * Gets the value of the informant property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the informant property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getInformant().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link POCDMT000040Informant12 }
	 *
	 *
	 */
	public List<POCDMT000040Informant12> getInformant() {
		if (informant == null) {
			informant = new ArrayList<POCDMT000040Informant12>();
		}
		return this.informant;
	}

	/**
	 * Gets the value of the interpretationCode property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the interpretationCode property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getInterpretationCode().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link CE }
	 *
	 *
	 */
	public List<CE> getInterpretationCode() {
		if (interpretationCode == null) {
			interpretationCode = new ArrayList<CE>();
		}
		return this.interpretationCode;
	}

	/**
	 * Ruft den Wert der languageCode-Eigenschaft ab.
	 *
	 * @return possible object is {@link CS }
	 *
	 */
	public CS getLanguageCode() {
		return languageCode;
	}

	/**
	 * Gets the value of the methodCode property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the methodCode property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getMethodCode().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link CE }
	 *
	 *
	 */
	public List<CE> getMethodCode() {
		if (methodCode == null) {
			methodCode = new ArrayList<CE>();
		}
		return this.methodCode;
	}

	/**
	 * Ruft den Wert der moodCode-Eigenschaft ab.
	 *
	 * @return possible object is {@link XActMoodDocumentObservation }
	 *
	 */
	public XActMoodDocumentObservation getMoodCode() {
		return moodCode;
	}

	/**
	 * Gets the value of the nullFlavor property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the nullFlavor property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getNullFlavor().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link String }
	 *
	 *
	 */
	public List<String> getNullFlavor() {
		if (nullFlavor == null) {
			nullFlavor = new ArrayList<String>();
		}
		return this.nullFlavor;
	}

	/**
	 * Gets the value of the participant property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the participant property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getParticipant().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link POCDMT000040Participant2 }
	 *
	 *
	 */
	public List<POCDMT000040Participant2> getParticipant() {
		if (participant == null) {
			participant = new ArrayList<POCDMT000040Participant2>();
		}
		return this.participant;
	}

	/**
	 * Gets the value of the performer property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the performer property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getPerformer().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link POCDMT000040Performer2 }
	 *
	 *
	 */
	public List<POCDMT000040Performer2> getPerformer() {
		if (performer == null) {
			performer = new ArrayList<POCDMT000040Performer2>();
		}
		return this.performer;
	}

	/**
	 * Gets the value of the precondition property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the precondition property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getPrecondition().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link POCDMT000040Precondition }
	 *
	 *
	 */
	public List<POCDMT000040Precondition> getPrecondition() {
		if (precondition == null) {
			precondition = new ArrayList<POCDMT000040Precondition>();
		}
		return this.precondition;
	}

	/**
	 * Ruft den Wert der priorityCode-Eigenschaft ab.
	 *
	 * @return possible object is {@link CE }
	 *
	 */
	public CE getPriorityCode() {
		return priorityCode;
	}

	/**
	 * Gets the value of the realmCode property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the realmCode property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getRealmCode().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link CS }
	 *
	 *
	 */
	public List<CS> getRealmCode() {
		if (realmCode == null) {
			realmCode = new ArrayList<CS>();
		}
		return this.realmCode;
	}

	/**
	 * Gets the value of the reference property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the reference property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getReference().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link POCDMT000040Reference }
	 *
	 *
	 */
	public List<POCDMT000040Reference> getReference() {
		if (reference == null) {
			reference = new ArrayList<POCDMT000040Reference>();
		}
		return this.reference;
	}

	/**
	 * Gets the value of the referenceRange property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the referenceRange property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getReferenceRange().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link POCDMT000040ReferenceRange }
	 *
	 *
	 */
	public List<POCDMT000040ReferenceRange> getReferenceRange() {
		if (referenceRange == null) {
			referenceRange = new ArrayList<POCDMT000040ReferenceRange>();
		}
		return this.referenceRange;
	}

	/**
	 * Ruft den Wert der repeatNumber-Eigenschaft ab.
	 *
	 * @return possible object is {@link IVLINT }
	 *
	 */
	public IVLINT getRepeatNumber() {
		return repeatNumber;
	}

	/**
	 * Gets the value of the specimen property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the specimen property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getSpecimen().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link POCDMT000040Specimen }
	 *
	 *
	 */
	public List<POCDMT000040Specimen> getSpecimen() {
		if (specimen == null) {
			specimen = new ArrayList<POCDMT000040Specimen>();
		}
		return this.specimen;
	}

	/**
	 * Ruft den Wert der statusCode-Eigenschaft ab.
	 *
	 * @return possible object is {@link CS }
	 *
	 */
	public CS getStatusCode() {
		return statusCode;
	}

	/**
	 * Ruft den Wert der subject-Eigenschaft ab.
	 *
	 * @return possible object is {@link POCDMT000040Subject }
	 *
	 */
	public POCDMT000040Subject getSubject() {
		return subject;
	}

	/**
	 * Gets the value of the targetSiteCode property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the targetSiteCode property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getTargetSiteCode().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link CD }
	 *
	 *
	 */
	public List<CD> getTargetSiteCode() {
		if (targetSiteCode == null) {
			targetSiteCode = new ArrayList<CD>();
		}
		return this.targetSiteCode;
	}

	/**
	 * Gets the value of the templateId property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the templateId property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getTemplateId().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link II }
	 *
	 *
	 */
	public List<II> getTemplateId() {
		if (templateId == null) {
			templateId = new ArrayList<II>();
		}
		return this.templateId;
	}

	/**
	 * Ruft den Wert der text-Eigenschaft ab.
	 *
	 * @return possible object is {@link ED }
	 *
	 */
	public ED getText() {
		return text;
	}

	/**
	 * Ruft den Wert der typeId-Eigenschaft ab.
	 *
	 * @return possible object is {@link POCDMT000040InfrastructureRootTypeId }
	 *
	 */
	public POCDMT000040InfrastructureRootTypeId getTypeId() {
		return typeId;
	}

	/**
	 * Gets the value of the value property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the value property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows: <pre>
	 *    getValue().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link ANY }
	 *
	 *
	 */
	public List<ANY> getValue() {
		if (value == null) {
			value = new ArrayList<ANY>();
		}
		return this.value;
	}

	/**
	 * Ruft den Wert der negationInd-Eigenschaft ab.
	 *
	 * @return possible object is {@link Boolean }
	 *
	 */
	public Boolean isNegationInd() {
		return negationInd;
	}

	/**
	 * Legt den Wert der code-Eigenschaft fest.
	 *
	 * @param value
	 *            allowed object is {@link CD }
	 *
	 */
	public void setCode(CD value) {
		this.code = value;
	}

	/**
	 * Legt den Wert der derivationExpr-Eigenschaft fest.
	 *
	 * @param value
	 *            allowed object is {@link ST }
	 *
	 */
	public void setDerivationExpr(ST value) {
		this.derivationExpr = value;
	}

	/**
	 * Legt den Wert der effectiveTime-Eigenschaft fest.
	 *
	 * @param value
	 *            allowed object is {@link IVLTS }
	 *
	 */
	public void setEffectiveTime(IVLTS value) {
		this.effectiveTime = value;
	}

	/**
	 * Legt den Wert der languageCode-Eigenschaft fest.
	 *
	 * @param value
	 *            allowed object is {@link CS }
	 *
	 */
	public void setLanguageCode(CS value) {
		this.languageCode = value;
	}

	/**
	 * Legt den Wert der moodCode-Eigenschaft fest.
	 *
	 * @param value
	 *            allowed object is {@link XActMoodDocumentObservation }
	 *
	 */
	public void setMoodCode(XActMoodDocumentObservation value) {
		this.moodCode = value;
	}

	/**
	 * Legt den Wert der negationInd-Eigenschaft fest.
	 *
	 * @param value
	 *            allowed object is {@link Boolean }
	 *
	 */
	public void setNegationInd(Boolean value) {
		this.negationInd = value;
	}

	/**
	 * Legt den Wert der priorityCode-Eigenschaft fest.
	 *
	 * @param value
	 *            allowed object is {@link CE }
	 *
	 */
	public void setPriorityCode(CE value) {
		this.priorityCode = value;
	}

	/**
	 * Legt den Wert der repeatNumber-Eigenschaft fest.
	 *
	 * @param value
	 *            allowed object is {@link IVLINT }
	 *
	 */
	public void setRepeatNumber(IVLINT value) {
		this.repeatNumber = value;
	}

	/**
	 * Legt den Wert der statusCode-Eigenschaft fest.
	 *
	 * @param value
	 *            allowed object is {@link CS }
	 *
	 */
	public void setStatusCode(CS value) {
		this.statusCode = value;
	}

	/**
	 * Legt den Wert der subject-Eigenschaft fest.
	 *
	 * @param value
	 *            allowed object is {@link POCDMT000040Subject }
	 *
	 */
	public void setSubject(POCDMT000040Subject value) {
		this.subject = value;
	}

	/**
	 * Legt den Wert der text-Eigenschaft fest.
	 *
	 * @param value
	 *            allowed object is {@link ED }
	 *
	 */
	public void setText(ED value) {
		this.text = value;
	}

	/**
	 * Legt den Wert der typeId-Eigenschaft fest.
	 *
	 * @param value
	 *            allowed object is {@link POCDMT000040InfrastructureRootTypeId
	 *            }
	 *
	 */
	public void setTypeId(POCDMT000040InfrastructureRootTypeId value) {
		this.typeId = value;
	}

}
