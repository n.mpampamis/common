/*
 * The authorship of this project and accompanying materials is held by medshare GmbH, Switzerland.
 * All rights reserved. https://medshare.net
 *
 * Source code, documentation and other resources have been contributed by various people.
 * Project Team: https://gitlab.com/ehealth-connector/api/wikis/Team/
 * For exact developer information, please refer to the commit history of the forge.
 *
 * This code is made available under the terms of the Eclipse Public License v1.0.
 *
 * Accompanying materials are made available under the terms of the Creative Commons
 * Attribution-ShareAlike 4.0 License.
 *
 * This line is intended for UTF-8 encoding checks, do not modify/delete: äöüéè
 *
 */
package org.ehealth_connector.common.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XmlUtil {

	public static Document getXmlDocument(InputStream inputStream)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		InputStreamReader inputReader = new InputStreamReader(inputStream, "UTF-8");
		InputSource inputSource = new InputSource(inputReader);
		inputSource.setEncoding("UTF-8");

		return builder.parse(inputSource);
	}

	public static void writeXmlDocumentToFile(Document fDoc, File out) throws Exception {
		// if file doesnt exists, then create it
		if (out.exists())
			out.delete();
		out.createNewFile();
		writeXmlDocumentToOutputStream(fDoc, new FileOutputStream(out));
	}

	public static void writeXmlDocumentToOutputStream(Document fDoc, OutputStream out)
			throws Exception {
		fDoc.setXmlStandalone(true);
		DOMSource docSource = new DOMSource(fDoc);
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.setOutputProperty(OutputKeys.INDENT, "no");
		// out.write("<?xml version=\"1.0\"
		// encoding=\"UTF-8\"?>".getBytes("UTF-8"));
		transformer.transform(docSource, new StreamResult(out));
		out.flush();
		out.close();
	}
}
