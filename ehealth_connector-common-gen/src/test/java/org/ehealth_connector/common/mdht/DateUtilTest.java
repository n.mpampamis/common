/*
 * The authorship of this project and accompanying materials is held by medshare GmbH, Switzerland.
 * All rights reserved. https://medshare.net
 *
 * Source code, documentation and other resources have been contributed by various people.
 * Project Team: https://gitlab.com/ehealth-connector/api/wikis/Team/
 * For exact developer information, please refer to the commit history of the forge.
 *
 * This code is made available under the terms of the Eclipse Public License v1.0.
 *
 * Accompanying materials are made available under the terms of the Creative Commons
 * Attribution-ShareAlike 4.0 License.
 *
 * This line is intended for UTF-8 encoding checks, do not modify/delete: äöüéè
 *
 */
package org.ehealth_connector.common.mdht;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Test of class DateUtil
 */
@Ignore
public class DateUtilTest {

	/**
	 * Method implementing
	 *
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#convertDate(java.util.Date)}
	 * .
	 */
	@Test
	public void testConvertDate() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#convertSXCM_TSToEurString(java.util.List)}
	 * .
	 */
	@Test
	public void testConvertSXCM_TSToEurString() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#createIVL_TSFromEuroDate(java.util.Date)}
	 * .
	 */
	@Test
	public void testCreateIVL_TSFromEuroDateDate() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#createIVL_TSFromEuroDate(java.util.Date, java.util.Date)}
	 * .
	 */
	@Test
	public void testCreateIVL_TSFromEuroDateDateDate() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#createIVL_TSFromEuroDate(java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public void testCreateIVL_TSFromEuroDateStringString() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#createIVL_TSFromEuroDateTime(java.util.Date)}
	 * .
	 */
	@Test
	public void testCreateIVL_TSFromEuroDateTime() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#createIVXB_TSFromDate(java.util.Date)}
	 * .
	 */
	@Test
	public void testCreateIVXB_TSFromDate() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#createIVXB_TSFromEuroDate(java.lang.String)}
	 * .
	 */
	@Test
	public void testCreateIVXB_TSFromEuroDate() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#createSTCM_TS(java.util.Date)}
	 * .
	 */
	@Test
	public void testCreateSTCM_TSDate() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#createSTCM_TS(java.util.Date, java.util.Date)}
	 * .
	 */
	@Test
	public void testCreateSTCM_TSDateDate() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#createDateTSFromEuroDate(java.util.Date)}
	 * .
	 */
	@Test
	public void testCreateTSFromEuroDate() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#createTSFromEuroDateStr(java.lang.String)}
	 * .
	 */
	@Test
	public void testCreateTSFromEuroDateStr() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#createUnknownLowHighTimeNullFlavor()}
	 * .
	 */
	@Test
	public void testCreateUnknownLowHighTimeNullFlavor() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#createUnknownTime(org.openhealthtools.mdht.uml.hl7.vocab.NullFlavor)}
	 * .
	 */
	@Test
	public void testCreateUnknownTime() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#date(java.lang.String)}
	 * .
	 */
	@Test
	public void testDate() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#dateAndTime(java.lang.String)}
	 * .
	 */
	@Test
	public void testDateAndTime() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#format(java.util.Date)}
	 * .
	 */
	@Test
	public void testFormat() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#formatDate(java.util.Date)}
	 * .
	 */
	@Test
	public void testFormatDate() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#formatDateCH(java.util.Date)}
	 * .
	 */
	@Test
	public void testFormatDateCH() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#formatDateTimeCh(java.util.Date)}
	 * .
	 */
	@Test
	public void testFormatDateTimeCh() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#nowAsDate()}.
	 */
	@Test
	public void testNowAsDate() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#nowAsTS()}.
	 */
	@Test
	public void testNowAsTS() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#parseDate(org.openhealthtools.mdht.uml.hl7.datatypes.TS)}
	 * .
	 */
	@Test
	public void testParseDate() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#parseDateToStr(org.openhealthtools.mdht.uml.hl7.datatypes.TS)}
	 * .
	 */
	@Test
	public void testParseDateToStr() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#parseDateyyyyMMdd(java.lang.String)}
	 * .
	 */
	@Test
	public void testParseDateyyyyMMdd() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#parseDateyyyyMMddHHmm(java.lang.String)}
	 * .
	 */
	@Test
	public void testParseDateyyyyMMddHHmm() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#parseDateyyyyMMddHHmmZ(java.lang.String)}
	 * .
	 */
	@Test
	public void testParseDateyyyyMMddHHmmZ() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#parseIVL_TSVDateTimeValue(org.openhealthtools.mdht.uml.hl7.datatypes.IVL_TS)}
	 * .
	 */
	@Test
	public void testParseIVL_TSVDateTimeValue() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.utils.DateUtilMdht#ts(java.util.Date)}.
	 */
	@Test
	public void testTs() {
		fail("Not yet implemented");
	}

}
