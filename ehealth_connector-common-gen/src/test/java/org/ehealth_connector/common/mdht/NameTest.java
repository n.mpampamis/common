/*
 * The authorship of this project and accompanying materials is held by medshare GmbH, Switzerland.
 * All rights reserved. https://medshare.net
 *
 * Source code, documentation and other resources have been contributed by various people.
 * Project Team: https://gitlab.com/ehealth-connector/api/wikis/Team/
 * For exact developer information, please refer to the commit history of the forge.
 *
 * This code is made available under the terms of the Eclipse Public License v1.0.
 *
 * Accompanying materials are made available under the terms of the Creative Commons
 * Attribution-ShareAlike 4.0 License.
 *
 * This line is intended for UTF-8 encoding checks, do not modify/delete: äöüéè
 *
 */
package org.ehealth_connector.common.mdht;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Test of class Name
 */
@Ignore
public class NameTest {

	/**
	 * Method implementing
	 *
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Name#copyMdhtPn()}.
	 */
	@Test
	public void testCopyMdhtPn() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Name#getCompleteName()}.
	 */
	@Test
	public void testGetCompleteName() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Name#getMdhtPn()}.
	 */
	@Test
	public void testGetMdhtPn() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Name#Name(org.openhealthtools.mdht.uml.hl7.datatypes.ON)}
	 * .
	 */
	@Test
	public void testNameON() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Name#Name(org.openhealthtools.mdht.uml.hl7.datatypes.PN)}
	 * .
	 */
	@Test
	public void testNamePN() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Name#Name(java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public void testNameStringString() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Name#Name(java.lang.String, java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public void testNameStringStringString() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Name#Name(java.lang.String, java.lang.String, java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public void testNameStringStringStringString() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Name#setFamilyName(java.lang.String)}
	 * .
	 */
	@Test
	public void testSetFamilyName() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Name#setGivenName(java.lang.String)}.
	 */
	@Test
	public void testSetGivenName() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Name#setPn(org.openhealthtools.mdht.uml.hl7.datatypes.PN)}
	 * .
	 */
	@Test
	public void testSetPn() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Name#setPrefix(java.lang.String)}.
	 */
	@Test
	public void testSetPrefix() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Name#setSuffix(java.lang.String)}.
	 */
	@Test
	public void testSetSuffix() {
		fail("Not yet implemented");
	}

}
