/*
 * The authorship of this project and accompanying materials is held by medshare GmbH, Switzerland.
 * All rights reserved. https://medshare.net
 *
 * Source code, documentation and other resources have been contributed by various people.
 * Project Team: https://gitlab.com/ehealth-connector/api/wikis/Team/
 * For exact developer information, please refer to the commit history of the forge.
 *
 * This code is made available under the terms of the Eclipse Public License v1.0.
 *
 * Accompanying materials are made available under the terms of the Creative Commons
 * Attribution-ShareAlike 4.0 License.
 *
 * This line is intended for UTF-8 encoding checks, do not modify/delete: äöüéè
 *
 */
package org.ehealth_connector.common.mdht;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Test of class Patient
 */
@Ignore
public class PatientTest {

	/**
	 * Method implementing
	 *
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#addAddress(org.ehealth_connector.common.mdht.Address)}
	 * .
	 */
	@Test
	public void testAddAddress() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#addId(org.ehealth_connector.common.mdht.Identificator)}
	 * .
	 */
	@Test
	public void testAddId() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#addName(org.ehealth_connector.common.mdht.Name)}
	 * .
	 */
	@Test
	public void testAddName() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#copyMdhtPatient()}.
	 */
	@Test
	public void testCopyMdhtPatient() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#copyMdhtPatientRole()}.
	 */
	@Test
	public void testCopyMdhtPatientRole() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#copyMdhtRecordTarget()}.
	 */
	@Test
	public void testCopyMdhtRecordTarget() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#getName()}.
	 */
	@Test
	public void testGetName() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#getNames()}.
	 */
	@Test
	public void testGetNames() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#Patient(org.ehealth_connector.common.mdht.Name, org.ehealth_connector.common.enums.AdministrativeGender, java.util.Date)}
	 * .
	 */
	@Test
	public void testPatientNameAdministrativeGenderDate() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#Patient(org.ehealth_connector.common.mdht.Name, org.ehealth_connector.common.enums.AdministrativeGender, java.util.Date, org.ehealth_connector.common.mdht.Identificator)}
	 * .
	 */
	@Test
	public void testPatientNameAdministrativeGenderDateIdentificator() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#Patient(org.openhealthtools.mdht.uml.cda.RecordTarget)}
	 * .
	 */
	@Test
	public void testPatientRecordTarget() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#setAdministrativeGender(org.ehealth_connector.common.enums.AdministrativeGender)}
	 * .
	 */
	@Test
	public void testSetAdministrativeGender() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#setBirthday(java.util.Date)}.
	 */
	@Test
	public void testSetBirthday() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#setDeceasedInd(java.lang.Boolean)}
	 * .
	 */
	@Test
	public void testSetDeceasedInd() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#setDeceasedTime(java.util.Date)}
	 * .
	 */
	@Test
	public void testSetDeceasedTime() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#setEmployeeOccupation(java.lang.String)}
	 * .
	 */
	@Test
	public void testSetEmployeeOccupation() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#setMothersMaidenName(java.lang.String)}
	 * .
	 */
	@Test
	public void testSetMothersMaidenName() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#setMultipleBirthInd(java.lang.Boolean)}
	 * .
	 */
	@Test
	public void testSetMultipleBirthInd() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#setMultipleBirthOrderNumber(java.lang.Integer)}
	 * .
	 */
	@Test
	public void testSetMultipleBirthOrderNumber() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#setNation(java.lang.String)}.
	 */
	@Test
	public void testSetNation() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#setReligiousAffiliation(java.lang.String)}
	 * .
	 */
	@Test
	public void testSetReligiousAffiliation() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Patient#setTelecoms(org.ehealth_connector.common.mdht.Telecoms)}
	 * .
	 */
	@Test
	public void testSetTelecoms() {
		fail("Not yet implemented");
	}

}
