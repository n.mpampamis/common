/*
 * The authorship of this project and accompanying materials is held by medshare GmbH, Switzerland.
 * All rights reserved. https://medshare.net
 *
 * Source code, documentation and other resources have been contributed by various people.
 * Project Team: https://gitlab.com/ehealth-connector/api/wikis/Team/
 * For exact developer information, please refer to the commit history of the forge.
 *
 * This code is made available under the terms of the Eclipse Public License v1.0.
 *
 * Accompanying materials are made available under the terms of the Creative Commons
 * Attribution-ShareAlike 4.0 License.
 *
 * This line is intended for UTF-8 encoding checks, do not modify/delete: äöüéè
 *
 */
package org.ehealth_connector.common.mdht;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Test of class Performer
 */
@Ignore
public class PerformerTest {

	/**
	 * Method implementing
	 *
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Performer#addAddress(org.ehealth_connector.common.mdht.Address)}
	 * .
	 */
	@Test
	public void testAddAddress() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Performer#addId(org.ehealth_connector.common.mdht.Identificator)}
	 * .
	 */
	@Test
	public void testAddId() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Performer#addName(org.ehealth_connector.common.mdht.Name)}
	 * .
	 */
	@Test
	public void testAddName() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Performer#copyMdhtPerfomer()}.
	 */
	@Test
	public void testCopyMdhtPerfomer() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Performer#getCompleteName()}.
	 */
	@Test
	public void testGetCompleteName() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Performer#getGln()}.
	 */
	@Test
	public void testGetGln() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Performer#getGlnAsIdentificator()}.
	 */
	@Test
	public void testGetGlnAsIdentificator() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Performer#Performer()}.
	 */
	@Test
	public void testPerformer() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Performer#Performer(org.ehealth_connector.common.mdht.Name)}
	 * .
	 */
	@Test
	public void testPerformerName() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Performer#Performer(org.ehealth_connector.common.mdht.Name, java.lang.String)}
	 * .
	 */
	@Test
	public void testPerformerNameString() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Performer#Performer(org.ehealth_connector.common.mdht.Organization)}
	 * .
	 */
	@Test
	public void testPerformerOrganization() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Performer#Performer(org.openhealthtools.mdht.uml.cda.Performer2)}
	 * .
	 */
	@Test
	public void testPerformerPerformer2() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Performer#setGln(java.lang.String)}.
	 */
	@Test
	public void testSetGln() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Performer#setOrganization(org.ehealth_connector.common.mdht.Organization)}
	 * .
	 */
	@Test
	public void testSetOrganization() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Performer#setTelecoms(org.ehealth_connector.common.mdht.Telecoms)}
	 * .
	 */
	@Test
	public void testSetTelecoms() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Performer#setTimeValue(java.util.Date)}
	 * .
	 */
	@Test
	public void testSetTimeValue() {
		fail("Not yet implemented");
	}

}
