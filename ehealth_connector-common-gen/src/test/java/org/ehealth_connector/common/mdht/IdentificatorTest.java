/*
 * The authorship of this project and accompanying materials is held by medshare GmbH, Switzerland.
 * All rights reserved. https://medshare.net
 *
 * Source code, documentation and other resources have been contributed by various people.
 * Project Team: https://gitlab.com/ehealth-connector/api/wikis/Team/
 * For exact developer information, please refer to the commit history of the forge.
 *
 * This code is made available under the terms of the Eclipse Public License v1.0.
 *
 * Accompanying materials are made available under the terms of the Creative Commons
 * Attribution-ShareAlike 4.0 License.
 *
 * This line is intended for UTF-8 encoding checks, do not modify/delete: äöüéè
 *
 */
package org.ehealth_connector.common.mdht;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Test of class Identificator
 */
@Ignore
public class IdentificatorTest {

	/**
	 * Method implementing
	 *
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Identificator#convertToIdentificator(org.ehealth_connector.common.mdht.Code)}
	 * .
	 */
	@Test
	public void testConvertToIdentificator() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Identificator#getIdentificator(java.util.List, java.lang.String)}
	 * .
	 */
	@Test
	public void testGetIdentificator() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Identificator#getIi()}.
	 */
	@Test
	public void testGetIi() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Identificator#Identificator(org.ehealth_connector.common.enums.CodeSystems, java.lang.String)}
	 * .
	 */
	@Test
	public void testIdentificatorCodeSystemsString() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Identificator#Identificator(org.openhealthtools.mdht.uml.hl7.datatypes.II)}
	 * .
	 */
	@Test
	public void testIdentificatorII() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Identificator#Identificator(java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public void testIdentificatorStringString() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Identificator#setExtension(java.lang.String)}
	 * .
	 */
	@Test
	public void testSetGetExtension() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Identificator#setRoot(java.lang.String)}
	 * .
	 */
	@Test
	public void testSetGetRoot() {
		fail("Not yet implemented");
	}

}
