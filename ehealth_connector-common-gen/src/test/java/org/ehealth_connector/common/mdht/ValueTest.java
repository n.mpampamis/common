/*
 * The authorship of this project and accompanying materials is held by medshare GmbH, Switzerland.
 * All rights reserved. https://medshare.net
 *
 * Source code, documentation and other resources have been contributed by various people.
 * Project Team: https://gitlab.com/ehealth-connector/api/wikis/Team/
 * For exact developer information, please refer to the commit history of the forge.
 *
 * This code is made available under the terms of the Eclipse Public License v1.0.
 *
 * Accompanying materials are made available under the terms of the Creative Commons
 * Attribution-ShareAlike 4.0 License.
 *
 * This line is intended for UTF-8 encoding checks, do not modify/delete: äöüéè
 *
 */
package org.ehealth_connector.common.mdht;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Test of class Value
 */
@Ignore
public class ValueTest {

	/**
	 * Method implementing
	 *
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Value#Value(org.openhealthtools.mdht.uml.hl7.datatypes.ANY)}
	 * .
	 */
	@Test
	public void testValueANY() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Value#Value(org.openhealthtools.mdht.uml.hl7.datatypes.CD)}
	 * .
	 */
	@Test
	public void testValueCD() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Value#Value(org.ehealth_connector.common.mdht.Code)}
	 * .
	 */
	@Test
	public void testValueCode() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Value#Value(double, double, org.ehealth_connector.common.enums.Ucum)}
	 * .
	 */
	@Test
	public void testValueDoubleDoubleUcum() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Value#Value(org.openhealthtools.mdht.uml.hl7.datatypes.PQ)}
	 * .
	 */
	@Test
	public void testValuePQ() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Value#Value(org.openhealthtools.mdht.uml.hl7.datatypes.RTO)}
	 * .
	 */
	@Test
	public void testValueRTO() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Value#Value(java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public void testValueStringString() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.ehealth_connector.common.mdht.Value#Value(java.lang.String, org.ehealth_connector.common.enums.Ucum)}
	 * .
	 */
	@Test
	public void testValueStringUcum() {
		fail("Not yet implemented");
	}

}
